package com.myyh.system.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "t_user_Avatar", indexes = {@Index(name = "id", columnList = "id", unique = true)})
public class Avatar {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "create_time", columnDefinition = "datetime DEFAULT NULL")
    private Date createTime;

    @Column(name = "path", columnDefinition = "varchar(255) DEFAULT NULL COMMENT '路径'")
    private String path;

    @Column(name = "real_name", columnDefinition = "varchar(255) DEFAULT NULL COMMENT '真实文件名'")
    private String realName;

    @Column(name = "size", columnDefinition = "varchar(255) DEFAULT NULL COMMENT '大小'")
    private String size;

}
