package com.myh.test;

import java.util.concurrent.LinkedBlockingQueue;

public class TaskThread extends Thread{

    //无界队列
    public  LinkedBlockingQueue queue = new LinkedBlockingQueue();

    //需要求的结果
    public long result = 0L;
    @Override
    public void run() {
        //执行任务 循环100次运算1000*1000的值
        System.out.println("=======任务开始");
        //获取当前系统时间
        long l = System.currentTimeMillis();
        //任务延迟时间 n秒
        long n = 60*60*36L;
        while(l+n >  System.currentTimeMillis()){
            for(int i = 0; i<1000; i++){
                result +=  10*10;
            }

            //doSomeThing
            try {
                Thread.sleep(12000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        System.out.println("=======任务结束");

    }
}
