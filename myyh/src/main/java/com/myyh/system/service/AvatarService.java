package com.myyh.system.service;

import cn.hutool.core.lang.Assert;
import com.myyh.system.dao.AvatarDao;
import com.myyh.system.dao.UserDao;
import com.myyh.system.pojo.Avatar;
import com.myyh.system.pojo.User;
import com.myyh.system.util.RequestHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;


@Service
@Slf4j
public class AvatarService {

    @Autowired
    private AvatarDao avatarDao;

    @Autowired
    private UserDao userDao;

    public Avatar info() {
        String name = RequestHolder.getHttpServletRequest().getUserPrincipal().getName();
        Assert.notNull(name,"获取用户头像失败！用户名为空");
        User user = userDao.findByUsername(name);
        Assert.notNull(user,"获取用户头像失败！用户不存在");
        return avatarDao.findById(user.getId()).get();
    }
}
