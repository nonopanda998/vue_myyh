package com.myyh.system.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModiyHistoryInfo {
    public String id;
    public String modifySubject;
    public String modifyContentBefor;
    public String modifyContentAfter;
}
