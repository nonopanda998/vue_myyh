package com.myyh.system.dao;

import com.myyh.system.pojo.Avatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AvatarDao extends JpaRepository<Avatar,Integer> , JpaSpecificationExecutor<Avatar> {

}
