package com.myyh.system.controller;

import com.myyh.system.annotation.Anonymous;
import com.myyh.system.annotation.LogInfo;
import com.myyh.system.pojo.Avatar;
import com.myyh.system.pojo.vo.ResultBean;
import com.myyh.system.service.AvatarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/avatar")
@RestController
public class AvatarController {
    @Autowired
    private AvatarService avatarService;


    @Anonymous
    @LogInfo("查询头像信息")
    @GetMapping(value = "/info")
    public ResultBean<Avatar> info() { return new ResultBean<Avatar>(avatarService.info()); }

}
