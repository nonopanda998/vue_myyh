package com.myyh.system.service;

import cn.hutool.core.lang.Assert;
import com.myyh.system.dao.CircleDao;
import com.myyh.system.pojo.Circle;
import com.myyh.system.pojo.vo.CircleQuery;
import com.myyh.system.pojo.vo.PageParameter;
import com.myyh.system.util.QueryHelp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CircleService {

	@Autowired
	private CircleDao circleDao;

	public List<Circle> getAll(PageParameter pageParameter) {
		CircleQuery searchKey =(CircleQuery) pageParameter.getSearchKey(CircleQuery.class);
		Specification<Circle> spec = new Specification<Circle>() {
			@Override
			public Predicate toPredicate(Root<Circle> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate predicate = QueryHelp.getPredicate(root, searchKey, cb);
				return predicate;
			}
		};
		//根据查询条件，获取所有符合条件圈子
		List<Circle> circleList = circleDao.findAll(spec);
		//为圈子的子节点赋值
		List<Circle> resoutList = new ArrayList<>();
		//ids应对搜索时，搜索结果无根节点情况；
		Set<Integer> ids = new HashSet<>();
		for (Circle cirOut : circleList) {
			if(cirOut.getPid() == 0 ){
				resoutList.add(cirOut);
			}
			for (Circle cirIn : circleList) {
				if(cirOut.getId().equals(cirIn.getPid())){
					if(cirOut.getChildren() == null){
						cirOut.setChildren(new ArrayList<>());
					}
					cirOut.getChildren().add(cirIn);
					ids.add(cirIn.getId());
				}
			}
		}
		//搜索结果无根节点时
		if(resoutList.size() == 0){
			resoutList = circleList.stream()
					.filter(cir -> !ids.contains(cir.getId()))
					.collect(Collectors.toList());
		}
		return resoutList;
	}

	/**
	 * 更新
	 * @param updateCir
	 * @return
	 */
	public boolean update(Circle updateCir) {
		Circle cir = circleDao.getOne(updateCir.getId());
		cir.setName(updateCir.getName());
		cir.setEnabled(updateCir.getEnabled());
		cir.setPid(updateCir.getPid());
		circleDao.save(cir);
		return true;
	}

	/**
	 * 获取圈子层级结构
	 * @return
	 */
	public List<Map<String, Object>> tree() {
		//获取所有一级圈子
		List<Circle> byPid = circleDao.findByPid(0);
		//过滤掉禁用的圈子
		List<Circle> resoutList = byPid.stream().filter(cir -> cir.getEnabled()).collect(Collectors.toList());
		return getCirTree(resoutList);
	}

	/**
	 * 递归获取子圈子map集合
	 * @param rootCir
	 * @return
	 */
	private List<Map<String,Object>> getCirTree(List<Circle> rootCir) {
		List<Map<String,Object>> list = new LinkedList<>();
		rootCir.forEach(cir -> {
					if (cir!=null){
						List<Circle> cirList = circleDao.findByPid(cir.getId());
						List<Circle> collect = cirList.stream().filter(item -> item.getEnabled()).collect(Collectors.toList());
						Map<String,Object> map = new HashMap<>(16);
						map.put("id",cir.getId());
						map.put("value",cir.getId());
						map.put("label",cir.getName());
						if(collect!=null && collect.size()!=0){
							map.put("children",getCirTree(collect));
						}
						list.add(map);
					}
				}
		);
		return list;
	}

	/**
	 * 新增圈子
	 * @param circle
	 * @return
	 */
	public Boolean addcircle(Circle circle) {
		circle.setCreateTime(new Date());
		circleDao.save(circle);
		return true;
	}

	/**
	 * 删除圈子及子圈子
	 * @param id
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public Boolean del(Integer id) {
		Circle cir = circleDao.getOne(id);
		Assert.notNull(cir,"删除圈子不存在");
		List<Circle> byPidList = circleDao.findByPid(id);
		HashSet set = new HashSet();
		HashSet<Circle> delSet = getDelCir(byPidList,set);
		delSet.add(cir);
		for (Circle delCir : delSet) {
			circleDao.deleteById(delCir.getId());
		}
		return true;
	}

	/**
	 * 递归获取需删除圈子
	 * @param byPidList
	 * @param resoutSet
	 * @return
	 */
	private HashSet<Circle> getDelCir(List<Circle> byPidList,HashSet<Circle> resoutSet) {
		for (Circle cir : byPidList) {
			resoutSet.add(cir);
			List<Circle> byPid = circleDao.findByPid(cir.getId());
			if(byPid.size()>0){
				getDelCir(byPid,resoutSet);
			}
		}
		return resoutSet;
	}

	/**
	 * 查询所有启用状态的圈子
	 * @return
	 */
	public List<Map<String, Object>> getallEnabledCirs() {
		List<Map<String,Object>> resoutList = new LinkedList<>();
		List<Circle> byEnabled = circleDao.findByEnabled(true);
		for (Circle cir : byEnabled) {
			Map<String,Object> map = new HashMap<>();
			map.put("value",cir.getId());
			map.put("label",cir.getName());
			resoutList.add(map);
		}
		return resoutList;
	}
}
